import {Injectable} from '@angular/core';
import {Dictionary, languages} from "../types";
import {ApiService} from "./api.service";
import {StorageService} from "./storage.service";

@Injectable({
  providedIn: 'root'
})
export class DictionaryService {

  private dictionary: Dictionary;
  private readonly DEFAULT_LANG = languages.en_US;

  get activeLanguage(): languages {
    return this.storageService.getLanguage();
  }

  set activeLanguage(lang: languages) {
    this.storageService.storeLanguage(lang);
  }

  constructor(private apiService: ApiService,
              private storageService: StorageService) {
    this.activeLanguage = this.DEFAULT_LANG;
  }

  init() {
    this.storageService.storeLanguage(this.DEFAULT_LANG);
    this.dictionary = this.apiService.fetchDictionary();
  }

  getDictionary(): Dictionary {
    return this.dictionary;
  }

  getActiveDictionary(): Array<string> {
    return this.dictionary[this.activeLanguage];
  }
}
