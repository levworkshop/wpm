import { Injectable } from '@angular/core';
import {languages} from "../types";

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private readonly languageKey = 'lang';
  private readonly successKey = 'success';
  private readonly timeKey = 'time';
  private readonly backspaceCounterKey = 'backspaces';

  storeLanguage(lang: languages) {
    localStorage.setItem(this.languageKey, lang);
  }

  getLanguage(): languages {
    return localStorage.getItem(this.languageKey) as languages;
  }

  storeClassResult(result: boolean) {
    localStorage.setItem(this.successKey, JSON.stringify(result));
  }

  getClassResult(): boolean {
    const result = localStorage.getItem(this.successKey);
    return JSON.parse(result);
  }

  storeTime(time: string) {
    localStorage.setItem(this.timeKey, time);
  }

  getTime(): string {
    return localStorage.getItem(this.timeKey);
  }

  storeBackspaceCounter(count: number) {
    localStorage.setItem(this.backspaceCounterKey, JSON.stringify(count));
  }

  getBackspaceCounter(): number {
    const count = localStorage.getItem(this.backspaceCounterKey);
    return JSON.parse(count);
  }
}
