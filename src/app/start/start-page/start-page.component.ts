import {Component, OnInit} from '@angular/core';
import {DictionaryService} from "../../core/services/dictionary.service";
import {languages} from "../../core/types";

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.scss']
})
export class StartPageComponent implements OnInit {

  languages: Array<string>;
  selectedLang: string;

  constructor(private dictionaryService: DictionaryService) {
  }

  ngOnInit(): void {
    this.selectedLang = this.dictionaryService.activeLanguage;
    this.languages = Object.keys(languages);
  }

  selectLanguage() {
    this.dictionaryService.activeLanguage = languages[this.selectedLang];
  }

}
