import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypingPageComponent } from './typing-page/typing-page.component';
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [TypingPageComponent],
  imports: [
    CommonModule,
    FormsModule,
  ]
})
export class TypingModule { }
