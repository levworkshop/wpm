import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypingPageComponent } from './typing-page.component';

describe('TypingPageComponent', () => {
  let component: TypingPageComponent;
  let fixture: ComponentFixture<TypingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
