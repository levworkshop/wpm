import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DictionaryService} from "../../core/services/dictionary.service";
import {StorageService} from "../../core/services/storage.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-typing-page',
  templateUrl: './typing-page.component.html',
  styleUrls: ['./typing-page.component.scss']
})
export class TypingPageComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('inputField') inputField;

  randomSentence: string;
  typedText = '';
  typingTimer: string;
  errors: boolean;
  private date = new Date();
  private timerInterval: number;
  private backspaceCounter = 0;

  constructor(private router: Router,
              private dictionaryService: DictionaryService,
              private storageService: StorageService) { }

  ngOnInit(): void {
    const dict = this.dictionaryService.getActiveDictionary();
    this.randomSentence = dict[Math.floor(Math.random() * dict.length)];
  }

  ngAfterViewInit() {
    this.inputField.nativeElement.focus();
  }

  ngOnDestroy() {
    this.stopTimer();
  }

  testCompletion() {
    if (this.typedText === this.randomSentence) {
      this.endClass();
    }
  }

  onType(event: KeyboardEvent) {
    if (this.classStarted()) {
      if (event.key === 'Enter') {
        this.endClass();
      }

      this.errors = this.randomSentence.indexOf(this.typedText) === -1;

      if (event.code == 'Backspace') {
        this.backspaceCounter++;
      }
    }

    else {
      event.preventDefault();
      if (event.key === 'Enter') {
        this.startTimer();
      }
    }
  }

  progressCss(letter: string, typed: string): string {
    return !typed || !this.classStarted() ? '' : letter === typed ? 'text-success' : 'text-danger';
  }

  stopTimer() {
    clearInterval(this.timerInterval);
    this.timerInterval = null;
  }

  startTimer() {
    this.date.setHours(0);
    this.date.setMinutes(0);
    this.date.setSeconds(0);

    this.timerInterval = setInterval(() => {
      this.date.setSeconds(this.date.getSeconds() + 1);
      this.typingTimer = this.date.toLocaleTimeString('en-GB');
    }, 1000);
  }

  classStarted(): boolean {
    return this.timerInterval > -1;
  }

  endClass() {
    this.stopTimer();
    this.storageService.storeClassResult(this.typedText === this.randomSentence);
    this.storageService.storeTime(this.typingTimer);
    this.storageService.storeBackspaceCounter(this.backspaceCounter);
    this.router.navigate(['scoreboard']).catch();
  }

}
