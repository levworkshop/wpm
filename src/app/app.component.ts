import {Component, OnInit} from '@angular/core';
import {DictionaryService} from "./core/services/dictionary.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private dictionaryService: DictionaryService) {
  }

  ngOnInit() {
    // just loading some static data so it doesn't matter but if loaded from the server better use CanActivate on the route.
    this.dictionaryService.init();
  }

}
